#ifndef GRIDMAP_H
#define GRIDMAP_H

#include <map>

#include "Grid.h"
#include "GridLegend.h"
#include "GridPoint.h"

/**
 * Class to make spatial queries
 * @tparam T
 */
template <class T>
class GridMap {
public:
    GridMap(GridMap& other);
    GridMap(GridMap&& other) noexcept ;
    GridMap(Grid<T> grid, GridLegend<T> legend, bool diagonalConnectivity);

    int getWidth();
    int getHeight();

    GridPoint<int> getSource();
    GridPoint<int> getDestination();
    map<GridPoint<int>::Direction, typename Grid<T>::Cell> getNextStep(GridPoint<int> pos);
    double getMoveCost(GridPoint<int> from, GridPoint<int>::Direction direction);
    Grid<T> drawPath(vector<GridPoint<int>> path);
private:
    Grid<T> _grid;
    GridLegend<T> _legend;
    bool _diagonalConnectivity;
};

template<class T>
GridMap<T>::GridMap(GridMap &other)
        : _grid(other._grid),
          _legend(other._legend),
          _diagonalConnectivity(other._diagonalConnectivity) {}

template<class T>
GridMap<T>::GridMap(GridMap &&other) noexcept
        : _grid(other._grid),
          _legend(other._legend),
          _diagonalConnectivity(other._diagonalConnectivity) {}

template<class T>
GridMap<T>::GridMap(Grid<T> grid, GridLegend<T> legend, bool diagonalConnectivity):
    _grid(grid), _legend(legend), _diagonalConnectivity(diagonalConnectivity) {}

#endif //GRIDMAP_H
