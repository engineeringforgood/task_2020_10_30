#ifndef SEARCH_H
#define SEARCH_H

#include <vector>

#include "GridPoint.h"
#include "GridMap.h"

/**
 * A* path finding algorithm.
 * @tparam T type of grid cell
 * @param gridMap
 * @return
 */
template<class T>
vector<GridPoint<int>> findPath(GridMap<T> gridMap);

#endif //SEARCH_H
