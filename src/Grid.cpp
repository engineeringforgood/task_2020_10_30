#include <tuple>
#include <algorithm>

#include "Grid.h"


template <class T>
Grid<T>::Grid(vector<vector<T>> state) {
    _height = state.size();
    _width = -1;
    for (const vector<T>& row : state) {
        if (_width == -1) {
            _width = row.size();
        } else if (_width != row.size()) {
            throw runtime_error("Inconsistent row length.");
        }
        _state.push_back(row);
    }
}

template <class T>
Grid<T>::Grid(T value, int width, int height) {
    if (width < 0 || height < 0) {
        throw runtime_error("Got negative grid size.");
    }
    _width = width;
    _height = height;
    _state = vector<vector<T>>(_height, vector<T>(_width, value));
}

template<class T>
int Grid<T>::getWidth() {
    return _width;
}

template<class T>
int Grid<T>::getHeight() {
    return _height;
}

template <class T>
T Grid<T>::get(const GridPoint<int>& pos) {
    return _state[pos.row()][pos.col()];
}

template <class T>
void Grid<T>::set(const GridPoint<int>& pos, T value) {
    _state[pos.row()][pos.col()] = value;
}

template <class T>
void Grid<T>::incr(const GridPoint<int>& pos) {
    _state[pos.row()][pos.col()]++;
}

template <class T>
void Grid<T>::decr(const GridPoint<int>& pos) {
    _state[pos.row()][pos.col()]--;
}

template<class T>
vector<GridPoint<int>> Grid<T>::findAll(T value, int n, bool fromEnd) {
    vector<GridPoint<int>> result;
    if (fromEnd) {
        int column_ind = -1;
        for (int row_ind = _height - 1; row_ind >= 0; --row_ind) {
            vector<T> row = _state[row_ind];
            auto it = find(row.rbegin(), row.rend(), value);
            if (it != row.rend()) {
                column_ind = _width - 1 - distance(row.rbegin(), it);
                result.emplace_back(column_ind, row_ind);
                if (n >= 0 && result.size() == n) {
                    break;
                }
            }
        }
    }
    else {
        int row_ind = -1, column_ind = -1;
        for (vector<T> row : _state) {
            ++row_ind;
            auto it = find(row.begin(), row.end(), value);
            if (it != row.end()) {
                column_ind = distance(row.begin(), it);
                result.emplace_back(column_ind, row_ind);
                if (n >= 0 && result.size() == n) {
                    break;
                }
            }
        }
    }
    return result;
}


template<class T>
map<GridPoint<int>::Direction, typename Grid<T>::Cell> Grid<T>::getNeighbours(GridPoint<int> pos, bool diagonalConnectivity) {
    map<GridPoint<int>::Direction, Cell> neighbours;
    vector<GridPoint<int>::Direction> directions {pos.U, pos.L, pos.R, pos.D};
    if (diagonalConnectivity) {
        directions.insert(directions.end(), {pos.UL, pos.UR, pos.DL, pos.DR});
    }
    for (GridPoint<int>::Direction direction : directions) {
        GridPoint<int> neighbour = pos.getNeighbour(direction);
        if (_isLegal(neighbour)) {
            neighbours.insert({direction, Cell(neighbour, get(neighbour))});
        }
    }
    return neighbours;
}


template <class T>
ostream& operator<<(ostream& os, const Grid<T>& dt) {
    for (const vector<T>& row : dt._state) {
        for (T el : row) {
            os << el;
        }
        os << endl;
    }
    return os;
}

template<class T>
bool Grid<T>::_isLegal(const GridPoint<int>& pos) {
    return (pos.row() >= 0 && pos.row() < _height)
        && (pos.col() >= 0 && pos.col() < _width);
}

template<class T>
Grid<T>::Grid(const Grid &other) : _state(other._state), _height(other._height), _width(other._width) {}


template class Grid<int>;
template class Grid<char>;
template ostream& operator<<(ostream&, const Grid<int>&);
template ostream& operator<<(ostream&, const Grid<char>&);