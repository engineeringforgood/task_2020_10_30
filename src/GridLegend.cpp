#include "GridLegend.h"


#include <cmath>
#include <stdexcept>


template<class T>
GridLegend<T>::GridLegend(T floor, T obstacle, T source, T destination, T path, double defaultImpassability) {
    _symbol = map<string, T>({{"floor",       floor },
                              {"obstacle",    obstacle},
                              {"source",      source},
                              {"destination", destination},
                              {"path",        path}});
    _impassability[floor] = defaultImpassability;
    _impassability[obstacle] = INFINITY;
    _impassability[source] = defaultImpassability;
    _impassability[destination] = defaultImpassability;
    _impassability[path] = defaultImpassability;
}

template<class T>
bool GridLegend<T>::isDirected() {
    return _symbol["source"] != _symbol["destination"];
}

template<class T>
void GridLegend<T>::addSymbol(string name, T symbol) {
    _symbol[name] = symbol;
}

template<class T>
T GridLegend<T>::getSymbol(string name) {
    try {
        return _symbol.at(name);
    } catch (const out_of_range &e) {
        return _symbol["obstacle"];
    }
}

template<class T>
void GridLegend<T>::setImpassability(T type, double impassability) {
    _impassability[type] = impassability;
}

template<class T>
double GridLegend<T>::getImpassability(T type) {
    try {
        return _impassability.at(type);
    } catch (const out_of_range &e) {
        return INFINITY;
    }
}

template struct GridLegend<int>;
template struct GridLegend<char>;
