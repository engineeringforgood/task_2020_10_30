#include <cmath>

#include "GridPoint.h"

using namespace std;


template<class T>
GridPoint<T> GridPoint<T>::getNeighbour(GridPoint::Direction direction) {
    GridPoint<T> neighbour(*this);
    switch (direction) {
        case UL:
            neighbour._y--; neighbour._x--;
            break;
        case U:
            neighbour._y--;
            break;
        case UR:
            neighbour._y--; neighbour._x++;
            break;
        case L:
            neighbour._x--;
            break;
        case S:
            break;
        case R:
            neighbour._x++;
            break;
        case DL:
            neighbour._y++; neighbour._x--;
            break;
        case D:
            neighbour._y++;
            break;
        case DR:
            neighbour._y++; neighbour._x++;
            break;
    }
    return neighbour;
}

template<class T>
double GridPoint<T>::distance(GridPoint<T> other, GridPoint::DistanceType type) const {
    switch (type) {
        case euclidean:
            return pow(pow(_x - other._x, 2)
                       + pow(_y - other._y, 2),
                       0.5);
        case manhattan:
            return  abs(_x - other._x)
                    + abs(_y - other._y);
    }
}

template<class Y>
ostream &operator<<(ostream &os, const GridPoint<Y> &dt) {
    os << '(' << dt.x() << ',' << dt.y() << ')';
    return os;
}

template ostream &operator<<(ostream &os, const GridPoint<int> &dt);
template ostream &operator<<(ostream &os, const GridPoint<char> &dt);