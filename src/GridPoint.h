#ifndef GRIDPOINT_H
#define GRIDPOINT_H

#include <ostream>

using namespace std;

/**
 * 2D point on discrete grid.
 * @tparam T type of coordinates
 */
template <class T>
class GridPoint {
private:
    T _x;
    T _y;
public:
    /**
     * Acronyms of UP, DOWN, LEFT, RIGHT, STAY.
     * Example: UL is UP-LEFT direction.
     */
    enum Direction {
        UL, U, UR,
        L, S,  R,
        DL, D, DR,
    };

    GridPoint(T col, T row) : _x(col), _y(row) {}
    GridPoint(const GridPoint& other) : _x(other._x), _y(other._y) {}
    T x() const { return _x; }
    T y() const { return _y; }
    T col() const { return _x; }
    T row() const { return _y; }

    /**
     * Returns neighbour one step in specified direction.
     * The convention of axes direction: x - right, y - down.
     * (0,0) is in left, top corner.
     * @param direction
     * @return neighbour point
     */
    GridPoint<T> getNeighbour(Direction direction);

    enum DistanceType { euclidean, manhattan };
    double distance(GridPoint<T> other, DistanceType type)  const;

    template<class Y>
    friend ostream& operator<<(ostream& os, const GridPoint<Y>& dt);
};


template <class T>
bool operator==(const GridPoint<T> & lhs, const GridPoint<T> & rhs) {
    return (lhs.x() == rhs.x()) && (lhs.y() == rhs.y());
}

template <class T>
bool operator!=(const GridPoint<T> & lhs, const GridPoint<T> & rhs) {
    return (lhs.x() != rhs.x()) || (lhs.y() != rhs.y());
}

template <class T>
bool operator<(const GridPoint<T> & lhs, const GridPoint<T> & rhs) {
    return (lhs.x() < rhs.x()) || ((lhs.x() == rhs.x()) && (lhs.y() < rhs.y()));
}


template class GridPoint<int>;
template class GridPoint<double>;

#endif //GRIDPOINT_H
