#include <iostream>
#include <fstream>

#include "PathSearch.h"

using namespace std;

template <class T>
Grid<T> loadGrid(const string& path, GridLegend<T> legend) {
    ifstream gridFile(path);
    if (!gridFile) {
        throw runtime_error("File '" + path + "' not found.");
    }
    string line;
    int width = 0, height = 0;
    vector<vector<T>> gridState;
    while (getline(gridFile, line)) {
        if (height == 0) {
            width = line.length();
        }
        ++height;
        if (line.length() > width) {
            // cut too long line to the size of the first line
            line.resize(width);
        } else if (line.length() < width) {
            // pad too short line to the size of first line
            line.insert(line.end(), width - line.length(), legend.getSymbol("obstacle"));
        }
        gridState.emplace_back(line.begin(), line.end());
    }
    gridFile.close();
    return Grid<T>(gridState);
}

int main(int argc, char* argv[]) {
    if (argc <= 1) {
        cout << "usage: path_finder.bin input_file" << endl;
        cout << "\tinput_file: text file describing query. Legend: '.' floor, '#' obstacle, 'X' start or finish" << endl;
        cout << "The output of program is graphic representation of found path with '+' char. If path not found, corresponding message will be printed." << endl;
        return 0;
    }
    GridLegend<char> legend('.', '#', 'X', 'X', '+');
    Grid<char> grid = loadGrid<char>(argv[1], legend);
    bool diagonalConnectivity = true;
    GridMap<char> gridMap(grid, legend, diagonalConnectivity);

    vector<GridPoint<int>> path = findPath(gridMap);

    if (!path.empty()) {
        cout << gridMap.drawPath(path) << endl;
    } else {
        cout << "no path found" << endl;
    }
    return 0;
}