#ifndef GRID_H
#define GRID_H

#include <vector>
#include <map>

#include "GridPoint.h"

using namespace std;

/**
 * The class representing state of 2D grid.
 * @tparam T the cell class
 */
template <class T>
class Grid {
public:
    /**
     * The single cell of grid.
     */
    struct Cell {
        GridPoint<int> pos;
        T value;

        Cell(const GridPoint<int>& pos, T value) : pos(pos), value(value) {}
        Cell(const Cell& other) : pos(other.pos), value(other.value) {}
    };

    explicit Grid(vector<vector<T>> state);
    Grid(T value, int width, int height);
    Grid(const Grid& grid);
    int getWidth();
    int getHeight();
    T get(const GridPoint<int>& pos);
    void set(const GridPoint<int>& pos, T value);
    void incr(const GridPoint<int>& pos);
    void decr(const GridPoint<int>& pos);

    /**
     * Find n elements with specific value.
     * @param value to search
     * @param n the maximal number of returned elements
     * @param fromEnd, true if start search from the end
     * @return vector of coordinates of satisfying elements
     */
    vector<GridPoint<int>> findAll(T value, int n = -1, bool fromEnd = false);

    /**
     * Returns cells around position.
     * @param pos coordinates of the cell
     * @param diagonalConnectivity, true if should return diagonal neighbours
     * @return
     */
    map<GridPoint<int>::Direction, Cell> getNeighbours(GridPoint<int> pos, bool diagonalConnectivity);

    template<class Y>
    friend ostream& operator<<(ostream& os, const Grid<Y>& dt);
private:
    int _width;
    int _height;
    vector<vector<T>> _state;

    bool _isLegal(const GridPoint<int>& point2D);
};

#endif //GRID_H
