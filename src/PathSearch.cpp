#include <queue>
#include <iostream>
#include <set>
#include <memory>
#include <map>

#include "PathSearch.h"

/**
 * Container for information about cell in search progress
 */
struct CellSearchProgress {
    GridPoint<int> pos;
    shared_ptr<CellSearchProgress> previous;
    double distFromSource;
    double distToDestination;

    CellSearchProgress(const GridPoint<int> &pos,
                       const GridPoint<int> &destination,
                       const shared_ptr<CellSearchProgress> &previous,
                       double stepCost = 1)
            : pos(pos), previous(previous) {
        if (!previous) {
            distFromSource = 0;
        } else {
            distFromSource = previous->distFromSource + stepCost;
        }
        distToDestination = pos.distance(destination, GridPoint<int>::euclidean);
    }

    friend ostream &operator<<(ostream &os, const CellSearchProgress &dt) {
        os << dt.pos << ' ' << dt.distFromSource << ':' << dt.distToDestination;
        return os;
    }
};

vector<GridPoint<int>> backPropagate(shared_ptr<CellSearchProgress> last) {
    vector<GridPoint<int>> path;
    while (last != nullptr) {
        path.push_back(last->pos);
        last = last->previous;
    }
    return path;
}

bool queueComparator(const shared_ptr<CellSearchProgress> &left,
                     const shared_ptr<CellSearchProgress> &right) {
    return left->distFromSource + left->distToDestination > right->distFromSource + right->distToDestination;
}

template<class T>
vector<GridPoint<int>> findPath(GridMap<T> gridMap) {
    priority_queue<shared_ptr<CellSearchProgress>, vector<shared_ptr<CellSearchProgress>>,
            decltype(&queueComparator)> current(queueComparator);
    set<GridPoint<int>> visited;

    GridPoint<int> source = gridMap.getSource();
    GridPoint<int> destination = gridMap.getDestination();

    vector<GridPoint<int>> path;
    if (source == destination) {
        return path;
    }

    shared_ptr<CellSearchProgress> sourceCell(new CellSearchProgress(source, destination, nullptr));
    current.push(sourceCell);
    while (!current.empty()) {
        shared_ptr<CellSearchProgress> cell = current.top();
        current.pop();
        visited.insert(cell->pos);

        // cout << cell->pos << endl;

        map<GridPoint<int>::Direction, Grid<char>::Cell> neighbours = gridMap.getNextStep(cell->pos);
        for (auto &it : neighbours) {
            GridPoint<int>::Direction direction = it.first;
            Grid<char>::Cell neighbour = it.second;
            if (visited.count(neighbour.pos) > 0) {
                continue; // skip visited neighbour cells
            }
            double stepCost = gridMap.getMoveCost(cell->pos, direction);
            shared_ptr<CellSearchProgress> neighbourCell(new CellSearchProgress(neighbour.pos,
                                                                                destination,
                                                                                cell,
                                                                                stepCost));
            // cout << *neighbourCell << endl;

            if (neighbour.pos == destination) {
                path = backPropagate(neighbourCell);
                return path;
            }
            current.push(neighbourCell);
            visited.insert(neighbour.pos);
        }
    }
    return path;
}

template vector<GridPoint<int>> findPath(GridMap<char> gridMap);
