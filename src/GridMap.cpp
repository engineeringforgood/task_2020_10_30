#include <cmath>
#include "GridMap.h"

template<class T>
int GridMap<T>::getWidth() {
    return _grid.getWidth();
}

template<class T>
int GridMap<T>::getHeight() {
    return _grid.getHeight();
}

template<class T>
GridPoint<int> GridMap<T>::getSource() {
    vector<GridPoint<int>> nodes = _grid.findAll(_legend.getSymbol("source"), 1, false);
    return nodes.front();
}

template<class T>
GridPoint<int> GridMap<T>::getDestination() {
    vector<GridPoint<int>> nodes = _grid.findAll(_legend.getSymbol("destination"), 1, true);
    return nodes.front();
}

template<class T>
map<GridPoint<int>::Direction, typename Grid<T>::Cell> GridMap<T>::getNextStep(GridPoint<int> pos) {
    map<GridPoint<int>::Direction, typename Grid<T>::Cell> neighbours
        = _grid.getNeighbours(pos, _diagonalConnectivity);
    // filter obstacles
    for (auto it = neighbours.cbegin(); it != neighbours.cend();) {
        if(_legend.getImpassability(it->second.value) == INFINITY) {
            it = neighbours.erase(it);
        } else {
            ++it;
        }
    }
    return neighbours;
}

template<class T>
double GridMap<T>::getMoveCost(GridPoint<int> from, GridPoint<int>::Direction direction) {
    return _legend.getImpassability(_grid.get(from.getNeighbour(direction)));
}

template<class T>
Grid<T> GridMap<T>::drawPath(vector<GridPoint<int>> path) {
    Grid<T> paintedGrid(_grid);
    const GridPoint<int> source = getSource();
    const GridPoint<int> destination = getDestination();
    for (const auto& step : path) {
        if (step != source && step != destination) {
            paintedGrid.set(step, _legend.getSymbol("path"));
        }
    }
    return paintedGrid;
}

template class GridMap<int>;
template class GridMap<char>;
