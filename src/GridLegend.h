#ifndef GRIDLEGEND_H
#define GRIDLEGEND_H

#include <cstring>
#include <map>

using namespace std;

/**
 * The legend of grid cell values
 * @tparam T
 */
template <class T>
class GridLegend {
public:
    GridLegend(T floor, T obstacle, T source, T destination, T path, double defaultImpassability = 1);
    bool isDirected();
    void addSymbol(string name, T symbol);
    T getSymbol(string name);
    void setImpassability(T type, double impassability);
    double getImpassability(T type);
private:
    map<string, T> _symbol;
    map<T, double> _impassability;
};


#endif //GRIDLEGEND_H
