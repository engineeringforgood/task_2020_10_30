## Описание
Программа для поиска пути в лабиринте.
Используется алгоритм поиска приблизительно
кратчайшего пути - A* ([объяснение](https://www.redblobgames.com/pathfinding/a-star/introduction.html)).
## Использование
```console
$ path_finder.bin input_file
```
* input_file: текстовый файл описывающий лабиринт.
## Входные данные
Лабиринт представляет из себя прямоугольную матрицу в текстовом файле.
Ширина поля вычисляется по длине первой строки.
Если последующие строки отличаются по длине,
то они либо обрезаются, либо дополняются клетками 
с препятсвием. Если на карте используются другие 
символы, они трактуются как препятствие. Если точек
входа и выхода обозначено больше чем две, используются
только близжайшие к верхнему-левому углу и к 
нижнему-правому (построчно).

Входной файл может иметь следующий вид:
```
.X...
.##.
...#
...X
```
## Результат
Найденный путь выводится в стандартный поток вывода в графическом виде.
Если путь не найден, выводится соответвующее сообщение.
```
.X+..
.##+#
..+##
...X#
```
## Обозначения
* '.' проходимая ячёйка
* '#' непроходимая ячейка (препятсвие)
* 'X' точка входа и точка выхода
* '+' путь
## Сборка из исходного кода
Для сборки необходимо открыть терминал в папке _src_ и выполнить:
```console
$ g++ -std=c++11 -opath_finder.bin *.h *.cpp
$ chmod u+x path_finder.bin
```
## Дизайн и структура кода
Из модуля **PathSearch** вызывается функция **findPath**,
которая принимает клеточную карту **GridMap** и возвращает
найденый путь в виде череды точек **GridPoint**. 
В **findPath** используется _алгоритм поиска пути A*_, который 
сортирует точки продвигающегося фронта поиска в 
соответствие с суммой пройденного растояния от 
начала и предполагаемым расстоянием до цели.

Класс **GridMap** включает в себя клеточное поле 
**Grid** и обозначения на этом поле **GridLegend**.
Так же **GridMap** принимает параметр типа 
[связанности](https://ru.qaz.wiki/wiki/Pixel_connectivity) 
соседних клеток поля:
* если считать соседними только клетки касающиеся сторонами (4-cвязная, [Окрестность
  фон Неймана](
  https://ru.wikipedia.org/wiki/%D0%9E%D0%BA%D1%80%D0%B5%D1%81%D1%82%D0%BD%D0%BE%D1%81%D1%82%D1%8C_%D1%84%D0%BE%D0%BD_%D0%9D%D0%B5%D0%B9%D0%BC%D0%B0%D0%BD%D0%B0))
  <br/>
  ![(Двумерная) окрестность фон Неймана порядка 1](https://upload.wikimedia.org/wikipedia/commons/f/f8/Nbhd_neumann_1.png)
* либо считать соседними так же клетки по диагонали (8-связная, [Окрестность Мура](
 https://ru.wikipedia.org/wiki/%D0%9E%D0%BA%D1%80%D0%B5%D1%81%D1%82%D0%BD%D0%BE%D1%81%D1%82%D1%8C_%D0%9C%D1%83%D1%80%D0%B0)) 
 <br/>![Двумерная окрестность Мура порядка 1.](https://upload.wikimedia.org/wikipedia/commons/8/8e/Nbhd_moore_1.png)
